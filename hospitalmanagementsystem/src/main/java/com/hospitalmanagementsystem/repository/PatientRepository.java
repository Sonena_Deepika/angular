package com.hospitalmanagementsystem.repository;



import org.springframework.data.repository.CrudRepository;

import com.hospitalmanagementsystem.model.Patient;

public interface PatientRepository extends CrudRepository<Patient,Long>{
	
	

}
